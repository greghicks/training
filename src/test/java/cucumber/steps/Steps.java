package cucumber.steps;

import cucumber.api.DataTable;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

import java.util.Map;

public class Steps {

    @Given("fire one (.*)")
    @When("fire two (.*)")
    @Then("test (.*)")
    public void runLine(String var, DataTable table){
        System.out.println(var);
        System.out.println(table);
    }

    @Given("^these items$")
    public void these_items(DataTable arg1) throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        // For automatic transformation, change DataTable to one of
        // List<YourType>, List<List<E>>, List<Map<K,V>> or Map<K,V>.
        // E,K,V must be a scalar (String, Integer, Date, enum etc)
        for( Map<String,String> row : arg1.asMaps(String.class, String.class)){
            System.out.println(row.get("Label")+ " " + row.get("Value"));
        }
    }

}
