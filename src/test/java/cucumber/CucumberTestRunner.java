package cucumber;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
    strict = true,
    features = {"src/test/resources/features"},
    plugin = {"json:target/cucumber/json/report.json",
            "html:target/cucumber/html",
            "usage:target/cucumber/usage",
            "pretty"},
    monochrome = false,
    //tags = {},
    glue = {"cucumber.steps"}
)
public class CucumberTestRunner {
}
