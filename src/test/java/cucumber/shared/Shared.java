package cucumber.shared;

import cucumber.api.DataTable;
import org.openqa.selenium.WebDriver;

import java.util.HashMap;
import java.util.Map;

public class Shared {

    enum System{ crm, portal };

    private static ThreadLocal<Map<System, WebDriver>> sharedDriver = new ThreadLocal<>();
    private static ThreadLocal<Map<String,DataTable>> sharedTable = new ThreadLocal<>();
    private static ThreadLocal<Map<String,String>> sharedVariable = new ThreadLocal<>();

    private static void initiateSharedDriverIfNull(){
        if(null == sharedDriver.get())
            sharedDriver.set(new HashMap<>());
    }

    private static void initiateSharedTableIfNull(){
        if(null == sharedTable.get())
            sharedTable.set(new HashMap<>());
    }

    private static void initiateSharedVariableIfNull(){
        if(null == sharedVariable.get())
            sharedVariable.set(new HashMap<>());
    }

    public static WebDriver getWebDriver(System system){
        initiateSharedDriverIfNull();
        return sharedDriver.get().get(system);
    }

    public static void setWebDriver(System system, WebDriver driver){
        initiateSharedDriverIfNull();
        sharedDriver.get().put(system, driver);
    }

    public static void quitDriver(System system){
        initiateSharedDriverIfNull();
        WebDriver w = sharedDriver.get().get(system);
        w.close();
        w.quit();
        sharedDriver.get().remove(system);
    }

    public static DataTable getDataTable(String tableName){
        initiateSharedTableIfNull();
        return sharedTable.get().get(tableName);
    }

    public static void setDataTable(String tableName, DataTable table){
        initiateSharedTableIfNull();
        sharedTable.get().put(tableName, table);
    }

    public static void removeSharedTable(String tableName){
        initiateSharedTableIfNull();
        sharedTable.get().remove(tableName);
    }

    public static void setVariable(String name, String value){
        initiateSharedVariableIfNull();
        sharedVariable.get().put(name, value);
    }

    public static String getVariable(String name){
        initiateSharedVariableIfNull();
        return sharedVariable.get().get(name);
    }

    public static void removeVariable(String name){
        initiateSharedVariableIfNull();
        sharedVariable.get().remove(name);
    }

    public static String getOrGenerateValue(String value){
        return "";
    }
}