Feature: testing

Scenario Outline: test
 * fire one <var1>
  | table four |

 * fire two <var2>
 | table two |

 * test <var3>
  | table three |

 Examples:
 | var1 | var2 | var3 |
 | one | two | three |
 | four | five | six |